[TryHackMe | The Return of the Yeti](https://tryhackme.com/room/adv3nt0fdbopsjcap)
# WLAN Password

*VanSpy.pcapng* in Wireshark öffnen und als *.pcap* abspeicher.

![save-as-pcap](save-as-pcap.png)
## Filter

Den Traffic können wir mit folgendem Filter aus das nötigste beschränken.
```wireshark
wlan.ta == 22:c7:12:c7:e2:35 || wlan.da == 22:c7:12:c7:e2:35 && eapol || wlan.fc.type_subtype == 0x0008
```

```
wlan.ta == 22:c7:12:c7:e2:35 || wlan.da == 22:c7:12:c7:e2:35
```

## aircrack-ng

*aircrack-ng* findet das WLAN Passwort mit der *rockyou* Wordlist in wenigen Sekunden

![aircrack-ng](aircrack-ng.png)

```bash
aircrack-ng decrypt1.pcap -w /usr/share/wordlist/rockyou.txt
Reading packets, please wait...
Opening decrypt1.pcap
Read 15120 packets.

   #  BSSID              ESSID                     Encryption

   1  22:C7:12:C7:E2:35  FreeWifiBFC               WPA (1 handshake)

Choosing first network as target.

Reading packets, please wait...
Opening decrypt1.pcap
Read 15120 packets.

1 potential targets

                               Aircrack-ng 1.7

      [00:00:01] 33035/10303727 keys tested (25191.35 k/s)

      Time left: 6 minutes, 47 seconds                           0.32%

                           KEY FOUND! [ Christmas ]


      Master Key     : A8 3F 1D 1D 1D 1F 2D 06 8E D4 47 CE E9 FD 3A AA
                       B2 86 42 89 FA F8 49 93 D7 C1 A0 29 97 3D 44 9F

      Transient Key  : 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
                       00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
                       00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
                       00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00

      EAPOL HMAC     : C1 0A 70 D9 65 94 5B 57 F2 98 8A E0 FC FD 2B 22

```

Um den WPA Traffic entschlüsseln zu können müssen wir aus der *SSID* und dem *Passwort* das *Pre-Shared-Secret* berechnen.
### WPA PSK berechnen

Das tool *wpa_passphrase* welches mit dem *wpa_supplicant* mitgeliefert wird kann dies für uns übernehmen.

![wpa_passphrase](wpa_passphrase.png)

Die Syntax ist "SSID" "Passwort"

```bash
wpa_passphrase FreeWifiBFC Christmas
network={
        ssid="FreeWifiBFC"
        #psk="Christmas"
        psk=a83f1d1d1d1f2d068ed447cee9fd3aaab2864289faf84993d7c1a029973d449f
}

```
#### WPA PSK
```
wpa_psk: a83f1d1d1d1f2d068ed447cee9fd3aaab2864289faf84993d7c1a029973d449f
```
# WLAN Traffic

Nachdem wir das *WPA PSK* in Wireshark hinterlegt haben. können wir den WLAN Traffic lesen.

![wireshark-psk](wireshark-psk.png)

## Filter
Der relevante Traffic lässt sich weiter filtern.

```wireshark
tcp.stream eq 1
```

![wireshark-wlan-traffic.png](wireshark-wlan-traffic.png)

```wireshark
ip.addr == 10.0.0.2 || ip.addr == 10.1.1.1
```

![](wireshark-ip-filter.png)

# PowerShell Session

Auf TCP Port *4444* finden wir eine unverschlüsselte Powershell Session

```wireshark
tcp.port == 4444
tcp.stream eq 1005
```

![](../Assets/Pasted%20image%2020231221134307.png)

Folgen wir dem TCP-Stream

![](../Assets/Pasted%20image%2020231221132354.png)

In der Session läd der Angreifer zunächst *mimikatz* auf den Ziel-Rechner und stiehlt anschließend einen private Key für *RDP*.

## Privaten Key sichern

Das Zertifikat ist base64 encodiert. Folglich müssen wir es zunächst decodieren und als *pfx* abspeichern.

```shell
cat key.base64 | base64 -d > key.pfx
```

Nun können wir uns mit dem Privaten Key aus dem pfx das RSA-Zertfikat erstellen.

```shell
openssl pkcs12 -in key.pfx -nocerts -out server_key.pem -nodes
openssl rsa -in server_key.pem -out server.key
```

Da der Private Key mit *mimikatz* exportiert wurde ist das Passwort ebenfalls *mimikatz*

![](../Assets/key.pfx)

![](../Assets/server.key)

![](../Assets/server_key.pem)
# RDP Session
