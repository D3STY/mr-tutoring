

- Unterstützung bei allgemeinen Fragen: Beantwortung von Fragen der neuen Schüler zu verschiedenen Themen.
- Technische Hilfe: Unterstützung bei technischen Problemen, sei es im Zusammenhang mit dem Schulsystem, der Online-Plattform oder anderen digitalen Werkzeugen.
- Orientierung bieten: Hilfestellung bei der Orientierung in den verschiedenen Tools, Quellen oder in digitalen Lernumgebungen.
- Gemeinschaftsaufbau: Förderung eines positiven Klassenklimas und Integration neuer Teilnehmer in die Klassengemeinschaft.
- Informationsvermittlung: Weitergabe von relevanten Informationen zu Stundenplänen, Veranstaltungen oder internen Abläufen.
- Anlaufstelle sein: Bereitstellung eines Ansprechpartners für die Anliegen und Bedenken der Teilnehmer.
- Teamarbeit: Zusammenarbeit mit anderen Tutoren und Lehrern, um einen reibungslosen Ablauf des Unterrichts sicherzustellen.
- Motivation fördern: Unterstützung der Teilnehmern, um ein positives Lernumfeld zu schaffen und ihre Motivation zu stärken.
- Kommunikation: Kommunikation zwischen Teilnehmer und Dozenten um einen effektiven Informationsfluss sicherzustellen.

Diese Stichpunkte sollten einen Überblick über die verschiedenen Aufgaben eines Klassentutors im Rahmen des Onboardings bieten.