# Unternemenskonzept

## **Allgemeine Anforderungen:**
- Verfügbarkeit: 99,9%
- Integrität: Schutz vor Datenmanipulation und unbefugtem Zugriff
- Vertraulichkeit: Schutz sensibler Daten vor unbefugtem Zugriff

## **Unternehmensaktivität:**
- Die Mutiny Corporation ist in der Fertigungsindustrie tätig und produziert hochwertige Elektronikkomponenten.

## **Anzahl Mitarbeiter:**
- 500 Mitarbeiter

## **Server, die aus dem Internet erreichbar sein sollen:**
1. Webserver für Unternehmenswebsite (Port 80, 443)
2. E-Mail-Server (Port 25, 587)
3. VPN-Server für sicheren Remote-Zugriff (Port 1194)
4. File Transfer Protocol (FTP)-Server für den Datenaustausch (Port 21)

## **Anwendungsserver:**
1. ERP-Server für die Unternehmensressourcenplanung
2. CRM-Server für das Kundenbeziehungsmanagement
3. Produktionssteuerungsserver
4. Datenbankserver für die Datenspeicherung

## **Datenlagerung:**
- Daten werden in einem dedizierten, hochsicheren Rechenzentrum gespeichert, das regelmäßig gesichert wird.

## **Ausstattung wie Drucker:**
- Zentrale Druckerserver für gemeinsamen Druckzugriff.
- Drucker in verschiedenen Abteilungen je nach Bedarf.

## **Rechner für Mitarbeiter:**
- Standard-Desktops für die meisten Mitarbeiter.
- Leistungsfähigere Workstations für technische Abteilungen und Entwickler.

## **Spezialrechner:**
- Grafikdesigner erhalten spezielle Workstations mit leistungsstarker Grafikkarte.
- Produktionssteuerungscomputer in der Fertigung.

##  **Home-Office-Anteil:**
- 30% der Mitarbeiter arbeiten im Home-Office.


| Unternehmensdaten                  | Netzwerkinfrastruktur                |
| ---------------------------------- | ------------------------------------ |
| **Fiktiver Name:** Mutiny Enterprise | **Allgemeine Anforderungen:**        |
| **Aktivität:** Elektronikproduktion | - Verfügbarkeit: 99,9%               |
| **Mitarbeiter:** 500                | - Integrität: Schutz vor Datenmanipulation und unbefugtem Zugriff      |
| **Server aus dem Internet erreichbar:** | **Anwendungsserver:**              |
|   1. Webserver (Port 80, 443)       |   1. ERP-Server                      |
|   2. E-Mail-Server (Port 25, 587)   |   2. CRM-Server                      |
|   3. VPN-Server (Port 1194)         |   3. Produktionssteuerungsserver    |
|   4. FTP-Server (Port 21)           |   4. Datenbankserver                |
| **Datenlagerung:**                  | **Ausstattung:**                     |
|   - Dediziertes, hochsicheres Rechenzentrum     |   - Druckerserver                    |
|   - Regelmäßige Backups                     |   - Abteilungsdrucker                |
| **Rechner für Mitarbeiter:**        | **Spezialrechner:**                  |
|   - Standard-Desktops               |   - Grafikdesigner-Workstations     |
|   - Leistungsfähige Workstations für technische Abteilungen   |   - Produktionssteuerungscomputer  |
| **Home-Office-Anteil:**          |   **Vertraulichkeit:**|
|  30% | - Zugriffsbeschränkungen für vertrauliche Daten |  

# Netzwerkplan


```shell
Internet
  |
Firewall
  |
VPN Server
  |
Intranet
  |
  +-------------+----------------+-----------------+
  |             |                |                 |
Webserver    Mail Server     Database Server   Application Servers
  |             |                |                 |
FTP Server Intrusion Detection   File Server       ERP Server

```

```shell
+-------------------+     +------------------------+     +------------------------+     +-------------------------+
|                   |     |                        |     |                        |     |                         |
|    Internet       |-----|     Firewall           |-----|   VPN Server           |-----|     Intranet            |
|                   |     |                        |     |                        |     |                         |
+-------------------+     +------------------------+     +------------------------+     +-------------------------+
        |                              |                             |                             |
        |                              |                             |                             |
+-------------------+      +------------------------+    +------------------------+     +-------------------------+
|                   |      |                        |    |                        |     |                         |
|   Webserver       |      |    Mail Server         |    |   Database Server      |     |   Application Servers   |
|                   |      |                        |    |                        |     |                         |
+-------------------+      +------------------------+    +------------------------+     +-------------------------+
        |                              |                             |                             |
        |                              |                             |                             |
+-------------------+      +------------------------+    +------------------------+     +-------------------------+
|                   |      |                        |    |                        |     |                         |
|   FTP Server      |      | Intrusion Detection    |    |   File Server          |     |    ERP Server           |
|                   |      | System                 |    |                        |     |                         |
+-------------------+      +------------------------+    +------------------------+     +-------------------------+

```


# Netzwerkkonzept

## Mutiny Enterprise - Netzwerkkonzept

### Sicherheitszonen und Subnetze:

1. **Internetzone:**
   - **Subnetz:** 203.0.113.0/24 (Öffentliche IP-Adressen für Internetressourcen)

2. **Perimeterzone (DMZ):**
   - **Subnetz:** 192.168.10.0/24 (DMZ-Subnetz mit strengen Zugriffskontrollen)

3. **Internes Netzwerk:**
   - **Subnetz:** 192.168.20.0/24 (Privates Subnetz für interne Geräte)

4. **Managementzone:**
   - **Subnetz:** 192.168.30.0/24 (Getrenntes Subnetz für Netzwerkmanagement)

5. **WLAN:**
   - **Subnetz:** 192.168.40.0/24 (WLAN für Mitarbeiter)

6. **Gast-WLAN:**
   - **Subnetz:** 192.168.50.0/24 (Gast-WLAN mit eingeschränktem Zugriff)

### VLANs:

1. **Default VLAN (VLAN 1):**
   - Allgemeine Büroanwendungen und Geräte im internen Netzwerk.

2. **DMZ VLAN (VLAN 10):**
   - Geräte in der DMZ.

3. **Management VLAN (VLAN 20):**
   - Netzwerkverwaltungsgeräte.

4. **WLAN VLAN (VLAN 30):**
   - Mitarbeiter-WLAN.

5. **Gast-WLAN VLAN (VLAN 40):**
   - Gast-WLAN.

#### VLAN-Konfiguration:

- Separates VLAN für WLAN (VLAN 30) und Gast-WLAN (VLAN 40).
- Isolation von VLANs, um den Datenverkehr zwischen Mitarbeitern und Gästen zu trennen.

### Kommunikationswege:

1. **Erlaubte Kommunikation:**
   - Innerhalb des internen Netzwerks und spezifizierten VLANs.
   - Zwischen DMZ und dem Internet.
   - Zwischen bestimmten VLANs und dem Internet.

2. **Blockierte Kommunikation:**
   - Direkte Kommunikation zwischen DMZ und internem Netzwerk.
   - Unnötiger Datenverkehr zwischen bestimmten VLANs.

### Authentifizierung und Zugriffskontrolle:

1. **WLAN:**
   - Verwendung von WPA2/WPA3 für die WLAN-Verschlüsselung.
   - Authentifizierung über EAP/RADIUS für sicheren Zugriff.

2. **Gast-WLAN:**
   - Captive Portal für Gast-Authentifizierung.
   - Separater Internetzugang, isoliert vom internen Netzwerk.

## Anforderungen:

1. **Allgemeine Anforderungen:**
   - Umfassende Netzwerksicherheitsrichtlinien und -prozeduren.
   - Regelmäßige Sicherheitsüberprüfungen und Audits.

2. **Verfügbarkeit:**
   - Redundante Internetverbindungen für kontinuierlichen Zugang.
   - Lastenausgleich für Server zur Maximierung der Verfügbarkeit.

3. **Integrität:**
   - Integritätsüberprüfungen für kritische Systeme.
   - Regelmäßige Datensicherungen und Überprüfung der Wiederherstellbarkeit.

4. **Vertraulichkeit:**
   - Einsatz von Verschlüsselung für sensible Datenübertragungen.
   - Zugriffsbeschränkungen und Verschlüsselung für gespeicherte Daten.

5. **Monitoring und Incident Response:**
   - Einrichtung von Überwachungstools für Netzwerkaktivitäten.
   - Klare Verfahren für den Umgang mit Sicherheitsvorfällen.

6. **Benutzerzugriffssteuerung:**
   - Strikte Zugriffskontrollen basierend auf Rollen.
   - Nutzung von Zwei-Faktor-Authentifizierung für kritische Systeme.

7. **Aktualisierungsmanagement:**
   - Regelmäßige Aktualisierung von Betriebssystemen, Anwendungen und Sicherheitspatches.
   - Überwachung und Aktualisierung von Firewalls und Sicherheitsgeräten.


| **Sicherheitszone**          | **Subnetz**               | **VLAN**  | **Geräte/Server**                                      |
|--------------------------|-----------------------|-------|-----------------------------------------------------|
| Internetzone             | 203.0.113.0/24        | -     | Firewall, Router, Public-Facing Server              |
| Perimeterzone (DMZ)      | 192.168.10.0/24       | VLAN 10 | Webserver, Mailserver, öffentliche Dienste          |
| Internes Netzwerk        | 192.168.20.0/24       | VLAN 1  | Mitarbeiter-Computer, interne Server               |
| Managementzone           | 192.168.30.0/24       | VLAN 20 | Netzwerkverwaltungsgeräte, Switches, Router        |


| **Authentifizierung**        | **Protokolle**            | **Dienste und Server**                                 |
|--------------------------|-----------------------|-----------------------------------------------------|
| EAP/RADIUS               | EAP, RADIUS           | RADIUS-Server, Authentifizierungsdienste           |
| Kerberos                 | Kerberos              | Kerberos-Server, Ticket Granting Server (TGS), Key Distribution Center (KDC)     |


```
 +----------------------------------------------------+
 |                      Internetzone                  |
 |                  +-----------------+               |
 |                  |   Public IP     |               |
 |                  |    Addresses    |               |
 |                  +-----------------+               |
 |                           |                        |
 |                           |                        |
 |                           |                        |
 |                           v                        |
 |                  +-----------------+               |
 |                  |   Firewall      |               |
 |                  +-----------------+               |
 |                           |                        |
 |                           |                        |
 +---------------------------|------------------------+
                             |
 +---------------------------|------------------------+
 |                           v                        |
 |                  +-----------------+               |
 |                  |  Perimeter Zone |               |
 |                  +-----------------+               |
 |                           |                        |
 |                           |                        |
 |                           |                        |
 |                           v                        |
 |                  +-----------------+               |
 |                  |      DMZ        |               |
 |                  | Subnet: 192.168.10.0/24         |
 |                  +-----------------+               |
 |                           |                        |
 |                           |                        |
 +---------------------------|------------------------+
                             |
 +---------------------------|------------------------+
 |                           v                        |
 |                  +-----------------+               |
 |                  | Internal Network|               |
 |                  | Subnet: 192.168.20.0/24         |
 |                  +-----------------+               |
 |                           |                        |
 |                           |                        |
 +---------------------------|------------------------+
                             |
 +---------------------------|------------------------+
 |                           v                        |
 |                  +-----------------+               |
 |                  | Management Zone |               |
 |                  | Subnet: 192.168.30.0/24         |
 |                  +-----------------+               |
 |                           |                        |
 |                           |                        |
 +---------------------------|------------------------+
                             |
                             v

                          +-----+
                          |Users|
                          +-----+
```

# Firewall Regeln

## Firewall-Regeln:

1. **Firewall zwischen Internetzone und Perimeterzone:**

   - Erlaube eingehenden Datenverkehr für Webserver (Port 80, 443):
     ```
     ALLOW IN: 203.0.113.0/24 -> 192.168.10.0/24 TCP 80, 443
     ```
   - Erlaube eingehenden Datenverkehr für Mailserver (z.B., Port 25):
     ```
     ALLOW IN: 203.0.113.0/24 -> 192.168.10.0/24 TCP 25
     ```
   - Erlaube ausgehenden Datenverkehr von DMZ ins Internet:
     ```
     ALLOW OUT: 192.168.10.0/24 -> 203.0.113.0/24 ANY
     ```
   - Blockiere direkten Datenverkehr von DMZ ins interne Netzwerk:
     ```
     DENY: 192.168.10.0/24 -> 192.168.20.0/24 ANY
     ```

2. **Firewall zwischen Perimeterzone und Internem Netzwerk:**

   - Blockiere eingehenden Datenverkehr von DMZ ins interne Netzwerk:
     ```
     DENY: 192.168.10.0/24 -> 192.168.20.0/24 ANY
     ```
   - Erlaube ausgehenden Datenverkehr vom internen Netzwerk ins Internet:
     ```
     ALLOW OUT: 192.168.20.0/24 -> 203.0.113.0/24 ANY
     ```
   - Blockiere direkten Datenverkehr von DMZ ins interne Netzwerk:
     ```
     DENY: 192.168.10.0/24 -> 192.168.20.0/24 ANY
     ```

3. **Firewall zwischen Internem Netzwerk und Managementzone:**

   - Erlaube bestimmten Datenverkehr für Netzwerkmanagement:
     ```
     ALLOW: 192.168.20.0/24 -> 192.168.30.0/24 TCP 22, 23, 161 (Beispielports)
     ```
   - Blockiere Datenverkehr von internem Netzwerk ins Managementnetzwerk:
     ```
     DENY: 192.168.20.0/24 -> 192.168.30.0/24 ANY
     ```

## nftables
```bash
# Erstelle eine neue Tabelle für IPv4
nft add table inet filter

# Regeln für die Internetzone
nft add chain inet filter internetzone { type filter hook input priority 0 \; }
nft add chain inet filter internetzone { type filter hook forward priority 0 \; }
nft add chain inet filter internetzone { type filter hook output priority 0 \; }

# Erlaube eingehenden Datenverkehr für Webserver (Port 80, 443)
nft add rule inet filter internetzone ip saddr 203.0.113.0/24 tcp dport {80, 443} accept

# Erlaube eingehenden Datenverkehr für Mailserver (Port 25)
nft add rule inet filter internetzone ip saddr 203.0.113.0/24 tcp dport 25 accept

# Erlaube ausgehenden Datenverkehr von DMZ ins Internet
nft add rule inet filter internetzone ip saddr 192.168.10.0/24 accept

# Blockiere direkten Datenverkehr von DMZ ins interne Netzwerk
nft add rule inet filter internetzone ip saddr 192.168.10.0/24 ip daddr 192.168.20.0/24 drop

# Regeln für die Perimeterzone
nft add chain inet filter perimeterzone { type filter hook input priority 0 \; }
nft add chain inet filter perimeterzone { type filter hook forward priority 0 \; }
nft add chain inet filter perimeterzone { type filter hook output priority 0 \; }

# Blockiere eingehenden Datenverkehr von DMZ ins interne Netzwerk
nft add rule inet filter perimeterzone ip saddr 192.168.10.0/24 ip daddr 192.168.20.0/24 drop

# Erlaube ausgehenden Datenverkehr vom internen Netzwerk ins Internet
nft add rule inet filter perimeterzone ip saddr 192.168.20.0/24 accept

# Blockiere direkten Datenverkehr von DMZ ins interne Netzwerk
nft add rule inet filter perimeterzone ip saddr 192.168.10.0/24 ip daddr 192.168.20.0/24 drop

# Regeln für das interne Netzwerk
nft add chain inet filter internalzone { type filter hook input priority 0 \; }
nft add chain inet filter internalzone { type filter hook forward priority 0 \; }
nft add chain inet filter internalzone { type filter hook output priority 0 \; }

# Erlaube bestimmten Datenverkehr für Netzwerkmanagement
nft add rule inet filter internalzone ip saddr 192.168.20.0/24 ip daddr 192.168.30.0/24 tcp dport {22, 23, 161} accept

# Blockiere Datenverkehr von internem Netzwerk ins Managementnetzwerk
nft add rule inet filter internalzone ip saddr 192.168.20.0/24 ip daddr 192.168.30.0/24 drop
```



# Netzwerkkonzept mit Cloud-Integration

### Mutiny Enterprise - Netzwerkkonzept

#### Sicherheitszonen und Subnetze:

1. **Internetzone:**
   - **Subnetz:** 203.0.113.0/24 (Öffentliche IP-Adressen für Internetressourcen)

2. **Perimeterzone (DMZ):**
   - **Subnetz:** 192.168.10.0/24 (DMZ-Subnetz mit strengen Zugriffskontrollen)

3. **Internes Netzwerk:**
   - **Subnetz:** 192.168.20.0/24 (Privates Subnetz für interne Geräte)

4. **Managementzone:**
   - **Subnetz:** 192.168.30.0/24 (Getrenntes Subnetz für Netzwerkmanagement)

5. **WLAN:**
   - **Subnetz:** 192.168.40.0/24 (WLAN für Mitarbeiter)

6. **Gast-WLAN:**
   - **Subnetz:** 192.168.50.0/24 (Gast-WLAN mit eingeschränktem Zugriff)

7. **Cloud-Zone:**
   - **Gmail (E-Mail):** Integration von Gmail für E-Mail-Dienste.
   - **Backup-Provider:** Integration eines Cloud-basierten Backup-Anbieters für Datensicherungen.
   - **VoIP (Sipgate):** Integration von Sipgate für die VoIP-Telefonie.

#### VLANs:

1. **Default VLAN (VLAN 1):**
   - Allgemeine Büroanwendungen und Geräte im internen Netzwerk.

2. **DMZ VLAN (VLAN 10):**
   - Geräte in der DMZ.

3. **Management VLAN (VLAN 20):**
   - Netzwerkverwaltungsgeräte.

4. **WLAN VLAN (VLAN 30):**
   - Mitarbeiter-WLAN.

5. **Gast-WLAN VLAN (VLAN 40):**
   - Gast-WLAN.

6. **Cloud VLAN (VLAN 50):**
   - VLAN für die Cloud-Integration mit Gmail, Backup-Provider und Sipgate.

#### Kommunikationswege:

1. **Erlaubte Kommunikation:**
   - Innerhalb des internen Netzwerks und spezifizierten VLANs.
   - Zwischen DMZ und dem Internet.
   - Zwischen bestimmten VLANs und dem Internet.
   - Zwischen internem Netzwerk und Cloud-Services (Gmail, Backup-Provider, Sipgate).

2. **Blockierte Kommunikation:**
   - Direkte Kommunikation zwischen DMZ und internem Netzwerk.
   - Unnötiger Datenverkehr zwischen bestimmten VLANs.
   - Einschränkung unnötiger Kommunikation zwischen Cloud-Services und internem Netzwerk.

#### Authentifizierung und Zugriffskontrolle:

1. **WLAN:**
   - Verwendung von WPA2/WPA3 für die WLAN-Verschlüsselung.
   - Authentifizierung über EAP/RADIUS für sicheren Zugriff.

2. **Gast-WLAN:**
   - Captive Portal für Gast-Authentifizierung.
   - Separater Internetzugang, isoliert vom internen Netzwerk.

3. **Cloud-Services:**
   - Authentifizierung und Zugriffskontrolle gemäß den Sicherheitsrichtlinien der jeweiligen Cloud-Anbieter.

#### VLAN-Konfiguration:

- Separates VLAN für WLAN (VLAN 30) und Gast-WLAN (VLAN 40).
- Isolation von VLANs, um den Datenverkehr zwischen Mitarbeitern und Gästen zu trennen.
- Cloud-VLAN (VLAN 50) für die Integration von Gmail, Backup-Provider und Sipgate.

#### Anforderungen:

1. **Allgemeine Anforderungen:**
   - Umfassende Netzwerksicherheitsrichtlinien und -prozeduren.
   - Regelmäßige Sicherheitsüberprüfungen und Audits.

2. **Verfügbarkeit:**
   - Redundante Internetverbindungen für kontinuierlichen Zugang.
   - Lastenausgleich für Server zur Maximierung der Verfügbarkeit.
   - Überwachung der Cloud-Services für maximale Verfügbarkeit.

3. **Integrität:**
   - Integritätsüberprüfungen für kritische Systeme.
   - Regelmäßige Datensicherungen und Überprüfung der Wiederherstellbarkeit.

4. **Vertraulichkeit:**
   - Einsatz von Verschlüsselung für sensible Datenübertragungen.
   - Zugriffsbeschränkungen und Verschlüsselung für gespeicherte Daten.

5. **Monitoring und Incident Response:**
   - Einrichtung von Überwachungstools für Netzwerkaktivitäten.
   - Klare Verfahren für den Umgang mit Sicherheitsvorfällen.
   - Überwachung der Cloud-Services für Sicherheitsvorfälle.

6. **Benutzerzugriffssteuerung:**
   - Strikte Zugriffskontrollen basierend auf Rollen.
   - Nutzung von Zwei-Faktor-Authentifizierung für kritische Systeme.
   - Umsetzung der Authentifizierungsmethoden der Cloud-Services.

7. **Aktualisierungsmanagement:**
   - Regelmäßige Aktualisierung von Betriebssystemen, Anwendungen und Sicherheitspatches.
   - Überwachung und Aktualisierung von Firewalls, Sicherheitsgeräten und Cloud-Konfigurationen.