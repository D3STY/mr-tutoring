*Checkliste für den Mitarbeiterlebenszyklus aus der Perspektive der IT-Sicherheit*

**1. Rekrutierung:**

- [ ] Festlegung von IT-Sicherheitsrichtlinien für neue Mitarbeiter
- [ ] Sicherstellung, dass der Rekrutierungsprozess Sicherheitsüberprüfungen einschließt
- [ ] Bereitstellung von Sicherheitsschulungen für neue Mitarbeiter
- [ ] Festlegung von Zugriffsberechtigungen basierend auf den Jobanforderungen

**2. Onboarding:**

- [ ] Einführung in die IT-Sicherheitsrichtlinien und -verfahren des Unternehmens
- [ ] Schulung zur sicheren Handhabung von Unternehmensdaten
- [ ] Einrichtung sicherer Zugangsdaten und Passwörter
- [ ] Implementierung von Multi-Faktor-Authentifizierung (MFA)

**3. Leistungsmanagement:**

- [ ] Regelmäßige Überprüfung der Zugriffsrechte und Sicherheitsberechtigungen
- [ ] Durchführung von Sicherheitsbewertungen als Teil der Leistungsüberprüfung
- [ ] Überwachung von Sicherheitsvorfällen und entsprechende Reaktionen

**4. Weiterbildung und Entwicklung:**

- [ ] Bereitstellung von kontinuierlichen Schulungen zu aktuellen Bedrohungen und Sicherheitspraktiken
- [ ] Förderung von Weiterbildungsmaßnahmen im Bereich IT-Sicherheit
- [ ] Aktualisierung von Schulungen zu Datenschutzrichtlinien und Compliance

**5. Mitarbeiterbindung:**

- [ ] Förderung eines Bewusstseins für IT-Sicherheit als gemeinsame Verantwortung
- [ ] Etablierung einer "See something, say something"-Kultur für Sicherheitsvorfälle
- [ ] Anerkennung von Mitarbeitern für ihre Beitrag zur Sicherheitskultur

**6. Konfliktmanagement:**

- [ ] Schulung der Mitarbeiter im Umgang mit Sicherheitsbedenken und Meldung von Vorfällen
- [ ] Klare Verfahren für den Umgang mit Sicherheitsverstößen und Konflikten
- [ ] Bereitstellung von Unterstützung bei Sicherheitsbedenken ohne negative Konsequenzen für den Melder

**7. Trennung/Entlassung:**

- [ ] Umgehende Deaktivierung von Zugriffsberechtigungen bei Austritt
- [ ] Rückgabe von Unternehmenseigentum, einschließlich IT-Geräten und Zugangskarten
- [ ] Überprüfung und Löschen aller Zugriffsrechte des ehemaligen Mitarbeiters
- [ ] Überwachung von verdächtigen Aktivitäten nach dem Ausscheiden

# English

*Checklist for the employee lifecycle from the perspective of IT security:*

**1. Recruitment:**

- [ ] Define IT security policies for new employees.
- [ ] Ensure that the recruitment process includes security checks.
- [ ] Provide security training for new employees.
- [ ] Set access permissions based on job requirements.

**2. Onboarding:**

- [ ] Introduce new employees to the company's IT security policies and procedures.
- [ ] Conduct training on the secure handling of company data.
- [ ] Set up secure access credentials and passwords.
- [ ] Implement Multi-Factor Authentication (MFA).

**3. Performance Management:**

- [ ] Regularly review access rights and security permissions.
- [ ] Include security assessments as part of performance reviews.
- [ ] Monitor security incidents and respond accordingly.

**4. Training and Development:**

- [ ] Provide continuous training on current threats and security best practices.
- [ ] Encourage professional development in the field of IT security.
- [ ] Update training on data privacy policies and compliance.

**5. Employee Engagement:**

- [ ] Promote awareness of IT security as a shared responsibility.
- [ ] Establish a "See something, say something" culture for security incidents.
- [ ] Recognize employees for their contributions to the security culture.

**6. Conflict Management:**

- [ ] Train employees on handling security concerns and reporting incidents.
- [ ] Have clear procedures for addressing security violations and conflicts.
- [ ] Provide support for security concerns without negative consequences for the reporter.

**7. Separation/Termination:**

- [ ] Immediately deactivate access permissions upon exit.
- [ ] Collect company property, including IT devices and access cards.
- [ ] Review and revoke all former employee access rights.
- [ ] Monitor for suspicious activities post-termination.
