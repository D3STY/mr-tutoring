Das NIST-Framework und die dazugehörigen Veröffentlichungen der 800-Serie werden in erster Linie von US-amerikanischen Bundesbehörden und Organisationen verwendet. Das National Institute of Standards and Technology (NIST) ist eine Agentur der US-Regierung und spielt eine zentrale Rolle bei der Entwicklung und Veröffentlichung von Standards und Richtlinien, insbesondere im Bereich der Informationssicherheit und des Risikomanagements.

Beispielsweise können Unternehmen in verschiedenen Branchen, die mit vertraulichen Informationen arbeiten oder regulatorischen Anforderungen unterliegen, die NIST-Richtlinien als Referenz für die Entwicklung ihrer Sicherheitsrichtlinien und -praktiken verwenden. 

1. **NIST 800-53:** Sicherheits- und Datenschutzkontrollen für föderale Informationssysteme und Organisationen.
2. **NIST 800-37:** Leitfaden zur Anwendung des Risikomanagement-Frameworks auf föderale Informationssysteme.
3. **NIST 800-30:** Leitfaden für die Durchführung von Risikobewertungen.
4. **NIST 800-171:** Schutz kontrollierter, unklassifizierter Informationen in nicht föderalen Systemen und Organisationen.
5. **NIST 800-63:** Digitale Identitätsrichtlinien.
6. **NIST 800-61:** Leitfaden für das Computer Security Incident Handling.
7. **NIST 800-171A:** Bewertung von Sicherheitsanforderungen für kontrollierte, unklassifizierte Informationen.
8. **NIST 800-66:** Ein einführender Ressourcenleitfaden zur Umsetzung des Sicherheitsregelwerks des Health Insurance Portability and Accountability Act (HIPAA).
9. **NIST 800-88:** Richtlinien zur Medien-Sanierung.
10. **NIST 800-161:** Praktiken für das Risikomanagement in der Lieferkette für föderale Informationssysteme und Organisationen.

# English

The NIST framework and the associated publications of the 800 series are primarily utilized by US federal agencies and organizations. The National Institute of Standards and Technology (NIST) is an agency of the US government and plays a central role in the development and publication of standards and guidelines, particularly in the fields of information security and risk management.

For instance, companies in various industries dealing with confidential information or subject to regulatory requirements can use NIST guidelines as a reference for the development of their security policies and practices.

1. **NIST 800-53:** Security and Privacy Controls for Federal Information Systems and Organizations.
2. **NIST 800-37:** Guide for Applying the Risk Management Framework to Federal Information Systems.
3. **NIST 800-30:** Guide for Conducting Risk Assessments.
4. **NIST 800-171:** Protecting Controlled Unclassified Information in Nonfederal Systems and Organizations.
5. **NIST 800-63:** Digital Identity Guidelines.
6. **NIST 800-61:** Computer Security Incident Handling Guide.
7. **NIST 800-171A:** Assessing Security Requirements for Controlled Unclassified Information.
8. **NIST 800-66:** An Introductory Resource Guide for Implementing the Health Insurance Portability and Accountability Act (HIPAA) Security Rule.
9. **NIST 800-88:** Guidelines for Media Sanitization.
10. **NIST 800-161:** Supply Chain Risk Management Practices for Federal Information Systems and Organizations.

