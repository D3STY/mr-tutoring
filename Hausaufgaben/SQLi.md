Eine SQL-Injection ist eine Angriffstechnik, bei der ein Angreifer bösartigen SQL-Code in Eingabefelder einer Anwendung einschleust, um unerlaubten Zugriff auf die Datenbank der Anwendung zu erhalten oder schädlichen Code auszuführen. Diese Art von Angriff ist möglich, wenn eine Anwendung Benutzereingaben nicht ordnungsgemäß filtert oder validiert, bevor sie in SQL-Abfragen eingebunden werden.

Hier ist eine Erklärung anhand des Beispiels "**OR 1=1 --**"

1. **OR 1=1**: In einer SQL-Abfrage wird normalerweise eine Bedingung verwendet, um bestimmte Datensätze auszuwählen. Das "OR 1=1" bedeutet, dass die Bedingung immer wahr ist (1=1 ist immer wahr). Dadurch wird die ursprüngliche Bedingung, die normalerweise dazu verwendet wird, bestimmte Daten zu filtern, umgangen. Das Ergebnis ist, dass alle Datensätze ausgewählt werden.

2. **--**: Dieser Teil ist ein Kommentar in SQL. Alles, was nach "--" steht, wird von der Datenbank nicht als Teil der Abfrage betrachtet. Das dient dazu, den Rest der ursprünglichen Abfrage zu "kommentieren" und zu deaktivieren.

Zusammengefasst würde eine typische Anfrage vor der Injektion so aussehen:

```sql
SELECT * FROM Benutzer WHERE Benutzername = 'Benutzer' AND Passwort = 'Passwort';
```

Nach der Injektion mit "OR 1=1 --" würde die Abfrage so aussehen:

```sql
SELECT * FROM Benutzer WHERE Benutzername = 'Benutzer' AND Passwort = 'Passwort' OR 1=1 --';
```

Da "1=1" immer true ist und der Rest der ursprünglichen Abfrage durch den Kommentar (--') deaktiviert wird, werden alle Datensätze in der Tabelle zurückgegeben, unabhängig von Benutzername und Passwort.

# Gegenmaßnahmen

Um die SQL-Injection "OR 1=1 --" zu verhindern, sollten Sie sicherstellen, dass Benutzereingaben ordnungsgemäß validiert und bereinigt werden, bevor sie in SQL-Abfragen eingefügt werden. Hier sind einige bewährte Methoden:

1. **Verwenden Sie Prepared Statements oder Parameterized Queries:**
   - In Java, PHP, .NET und vielen anderen Programmiersprachen können Sie vorbereitete Anweisungen (Prepared Statements) oder parameterisierte Abfragen verwenden. Diese Methoden binden Parameter sicher an die Abfrage und verhindern, dass Benutzereingaben als Teil des SQL-Codes interpretiert werden.

   Beispiel in Java:

   ```java
   String query = "SELECT * FROM Benutzer WHERE Benutzername = ? AND Passwort = ?";
   PreparedStatement pstmt = connection.prepareStatement(query);
   pstmt.setString(1, enteredUsername);
   pstmt.setString(2, enteredPassword);
   ResultSet results = pstmt.executeQuery();
   ```

2. **Input Validation:**
   - Validieren Sie alle Benutzereingaben, um sicherzustellen, dass sie den erwarteten Datentyp und das erwartete Format haben. Verwerfen Sie alle unzulässigen Zeichen oder Muster.

   Beispiel in Java:

   ```java
   // Validierung für Benutzernamen (Beispiel)
   if (enteredUsername.matches("[a-zA-Z0-9]+")) {
       // gültiger Benutzername
   } else {
       // ungültiger Benutzername
   }
   ```

