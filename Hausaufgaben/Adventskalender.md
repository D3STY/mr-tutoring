  1. **Frage 1:**
   *Welche der folgenden Aussagen trifft am besten auf stateful Firewalls zu?*

   - a) Sie analysieren den Datenverkehr anhand des Verbindungszustands.
     
   - b) Sie blockieren den Datenverkehr basierend auf festgelegten Regeln.
     
   - c) Sie arbeiten hauptsächlich auf der Anwendungsschicht.
     
   - d) Sie sind besonders effektiv bei der Blockierung von Denial-of-Service-Angriffen.
     
 

2. **Frage 2:**
   *Welche Funktionen werden von Intrusion Detection Systems (IDS) typischerweise durchgeführt?*

   - a) Echtzeitblockierung von Angriffen
     
   - b) Überwachung des Netzwerkverkehrs auf verdächtige Aktivitäten
     
   - c) Aktive Verschlüsselung von Datenübertragungen
     
   - d) Management von Firewall-Richtlinien
     
  

## Lösungen:  

**Frage 1:**
*Welche der folgenden Aussagen trifft am besten auf stateful Firewalls zu?*
- **Antwort:** a) Sie analysieren den Datenverkehr anhand des Verbindungszustands.

**Frage 2:**
*Welche Funktionen werden von Intrusion Detection Systems (IDS) typischerweise durchgeführt?*
- **Antwort:** b) Überwachung des Netzwerkverkehrs auf verdächtige Aktivitäten.