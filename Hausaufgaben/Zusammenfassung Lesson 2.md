
### Topic 2A: Bedrohungsakteure

| **Unterpunkte**               | **Beschreibung**                                        |
|-------------------------------|---------------------------------------------------------|
| Einführung                    | Videoanleitung vorhanden                                |
| Verwundbarkeit, Bedrohung, Risiko | Grundkonzepte verstehen                                |
| Attribute von Bedrohungsakteuren | Charakteristika von Angreifern                          |
| Motivationen von Bedrohungsakteuren | Warum Angreifer handeln                                |
| Hacker und Hacktivisten        | Unterschiede und Ziele                                  |
| Nationale Akteure              | Staatsgestützte Angreifer                               |
| Organisierte Kriminalität und Wettbewerber | Kriminelle Gruppen und Wettbewerbsmotivationen        |
| Interne Bedrohungsakteure      | Risiken aus den eigenen Reihen verstehen                |

### Topic 2B: Angriffsflächen

| **Unterpunkte**               | **Beschreibung**                                        |
|-------------------------------|---------------------------------------------------------|
| Einführung                    | Grundlegende Konzepte von Angriffsflächen                |
| Angriffsflächen und Bedrohungsvektoren | Verschiedene Eintrittspunkte für Angriffe verstehen    |
| Anfällige Softwarevektoren     | Schwachstellen in Software identifizieren               |
| Netzwerkvektoren               | Angriffsmöglichkeiten über Netzwerke                    |
| Lockvogelbasierte Vektoren    | Täuschungen als Angriffsvektor                          |
| Nachrichtenbasierte Vektoren  | Angriffe über Nachrichten verstehen                    |
| Angriffsfläche der Lieferkette | Risiken in der Lieferkette analysieren                  |

### Topic 2C: Soziale Ingenieurskunst

| **Unterpunkte**               | **Beschreibung**                                        |
|-------------------------------|---------------------------------------------------------|
| Einführung                    | Videoanleitung vorhanden                                |
| Menschliche Vektoren          | Einbeziehung menschlicher Schwächen                    |
| Imitation und Vorwandbildung  | Techniken wie Imitation und Vorwandbildung verstehen    |
| Phishing und Pharming         | Täuschungsmanöver durch gefälschte Kommunikation        |
| Typosquatting                 | Missbrauch von Tippfehlern für Angriffe                 |
| Business Email Compromise     | Angriffe durch Kompromittierung von Geschäfts-E-Mails   |
