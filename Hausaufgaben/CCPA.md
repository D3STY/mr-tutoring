- **CCPA (California Consumer Privacy Act):**
  - Datenschutzgesetz in Kalifornien, USA
  - In Kraft getreten am 1. Januar 2020
  - Gewährt Verbrauchern mehr Kontrolle über ihre persönlichen Daten

- **Hauptbestimmungen der CCPA:**
  - Recht auf Information über gesammelte persönliche Daten
  - Recht auf Löschung persönlicher Daten
  - Widerspruchsrecht gegen Weitergabe von Daten an Dritte
  - Recht, nicht diskriminiert zu werden bei Ausübung von Datenschutzrechten

- **Gilt für Unternehmen, die bestimmte Kriterien erfüllen:**
  - In Kalifornien tätig
  - Erreichen bestimmten Umsatz oder verarbeiten große Mengen persönlicher Daten

- **Einfluss auf Unternehmen:**
  - Erfordert Transparenz über Datensammlung, -verwendung und -weitergabe

- **Wichtig für Unternehmen, die CCPA einhalten müssen:**
  - Überprüfung aktueller Bestimmungen und Anforderungen
  - Datenschutzgesetze können sich weiterentwickeln.

# Im vergleich zur DSGVO

Die CCPA (California Consumer Privacy Act) und die DSGVO (Datenschutz-Grundverordnung) sind beide Datenschutzgesetze, jedoch gelten sie in unterschiedlichen geografischen Regionen und haben einige Unterschiede in Bezug auf ihren Anwendungsbereich, ihre Bestimmungen und ihre Auswirkungen. Hier sind einige Hauptunterschiede:

1. **Geografischer Geltungsbereich:**
   - **CCPA:** Gilt speziell für Unternehmen, die in Kalifornien tätig sind und bestimmte Kriterien erfüllen.
   - **DSGVO:** Gilt für alle Unternehmen, die personenbezogene Daten von EU-Bürgern verarbeiten, unabhängig von ihrem Standort.

2. **Rechte der betroffenen Personen:**
   - **CCPA:** Legt Rechte wie das Recht auf Information, das Recht auf Löschung und das Recht auf Widerspruch gegen die Weitergabe von Daten fest.
   - **DSGVO:** Umfasst ähnliche Rechte wie das Recht auf Auskunft, das Recht auf Löschung, das Recht auf Datenübertragbarkeit und das Widerspruchsrecht.

3. **Opt-in/Opt-out-Anforderungen:**
   - **CCPA:** Bietet Verbrauchern das Recht, der Weitergabe ihrer Daten an Dritte zu widersprechen (Opt-out).
   - **DSGVO:** Erfordert in vielen Fällen eine ausdrückliche Zustimmung der betroffenen Person (Opt-in) für die Verarbeitung personenbezogener Daten.

4. **Durchsetzungsmechanismen und Strafen:**
   - **CCPA:** Enthält Strafen für Verstöße, die von den kalifornischen Behörden durchgesetzt werden können. Betroffene Personen können auch individuelle Klagen einreichen.
   - **DSGVO:** Ermächtigt Datenschutzbehörden in den EU-Mitgliedstaaten zur Durchsetzung. Es können empfindliche Geldstrafen verhängt werden.

5. **Definition von personenbezogenen Daten:**
   - **CCPA:** Bietet eine breite Definition von personenbezogenen Daten.
   - **DSGVO:** Bietet eine umfassendere Definition, die auch besondere Kategorien von personenbezogenen Daten (wie Gesundheitsdaten) einschließt.