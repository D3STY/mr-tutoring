Die Kategorie, zuvor als "Broken Authentication" bekannt, ist von der zweiten Position abgerutscht und umfasst jetzt Common Weakness Enumerations (CWEs) im Zusammenhang mit Identifikationsfehlern. Zu den bemerkenswerten CWEs gehören CWE-297: Unsachgemäße Validierung von Zertifikaten mit Host-Mismatch, CWE-287: Unsachgemäße Authentifizierung und CWE-384: Session Fixation.

Die Bestätigung der Benutzeridentität, Authentifizierung und Sitzungsverwaltung ist entscheidend, um sich vor Angriffen im Zusammenhang mit der Authentifizierung zu schützen. Es können Authentifizierungsschwächen auftreten, wenn die Anwendung:

- Automatisierte Angriffe wie Credential Stuffing erlaubt, bei denen der Angreifer eine Liste gültiger Benutzernamen und Passwörter hat.
- Brute-Force- oder andere automatisierte Angriffe zulässt.
- Standard-, schwache oder bekannte Passwörter zulässt, wie z. B. "Password1" oder "admin/admin".
- Schwache oder ineffektive Prozesse zur Passwortwiederherstellung und zum Zurücksetzen verwendet, z. B. "wissensbasierte Antworten", die nicht sicher gemacht werden können.
- Passwörter in Klartext, verschlüsselt oder schwach gehasht in Datenbanken speichert.

Präventionsmaßnahmen:

- Implementierung von Multi-Faktor-Authentifizierung, um automatisierte Angriffe zu verhindern.
- Kein Versand oder Einsatz