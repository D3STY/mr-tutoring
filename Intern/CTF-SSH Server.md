
Hier sind allgemeine Schritte, um einen einfachen virtuellen SSH-Server mit einem Benutzer im *chroot* zu erstellen:

1. **Benötigte Pakete installieren:**
   Stellen Sie sicher, dass Sie die erforderlichen Pakete installiert haben. In der Regel benötigen Sie das `openssh-server`-Paket und möglicherweise `chroot`-Pakete und `fail2ban`-Pakete.

   ```bash
   sudo apt-get update
   sudo apt-get install -y openssh-server fail2ban chroot ufw php-fpm nginx certbot
   ```

2. **Benutzer erstellen:**
   Erstellen Sie einen Benutzer, den Sie für die Challenge verwenden möchten.

   ```bash
   sudo adduser ctfuser
   ```

3. **Chroot-Umgebung erstellen:**
   Erstellen Sie eine Chroot-Umgebung für den Benutzer. Kopieren Sie die benötigten Binärdateien und Bibliotheken in das Chroot-Verzeichnis.

   ```bash
   sudo mkdir -p /chroot/ctfuser
   sudo mkdir -p /chroot/ctfuser/bin
   sudo cp /bin/bash /chroot/ctfuser/bin/
   sudo cp /bin/nano /chroot/ctfuser/bin/
   sudo cp /bin/cat /chroot/ctfuser/bin/
   sudo cp /bin/gpg /chroot/ctfuser/bin/
   sudo cp /bin/unzip /chroot/ctfuser/bin/
   sudo cp /lib/x86_64-linux-gnu/libc.so.6 /chroot/ctfuser/lib/x86_64-linux-gnu/
   ```

4. **SSH-Konfiguration anpassen:**
   Passen Sie die SSH-Konfigurationsdatei an, um den Chroot für den Benutzer zu aktivieren.

   ```bash
   sudo nano /etc/ssh/sshd_config
   ```

   Fügen Sie am Ende der Datei hinzu:

   ```plaintext
   Match User ctfuser
   ChrootDirectory /chroot/ctfuser
   ```

Um einen sicheren SSH-Server zu konfigurieren, der Root-Anmeldungen verbietet, nur Schlüssel-basierte Authentifizierung erlaubt, aktuelle Chiffren verwendet und mögliche Angriffspunkte minimiert, können Sie die folgenden Konfigurationsempfehlungen verwenden. Bearbeiten Sie die SSH-Konfigurationsdatei normalerweise unter `/etc/ssh/sshd_config`:

```plaintext
# /etc/ssh/sshd_config

# Allgemeine Einstellungen
Port 22                       # Ändern Sie den Port auf einen benutzerdefinierten Wert (nicht 22) für zusätzliche Sicherheit
Protocol 2                    # Nur das SSH-Protokoll 2 zulassen

# Sicherheitsverbesserungen
PermitRootLogin yes           # Root-Anmeldungen verbieten
PasswordAuthentication yes    # Password Authentifizierung erlauben
PermitEmptyPasswords no       # Leere Passwörter verbieten
PermitUserEnvironment no      # Keine Benutzerumgebungen für Authentifizierung zulassen

# Chiffralgorithmen
Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr
MACs hmac-sha2-256-etm@openssh.com,hmac-sha2-512-etm@openssh.com,hmac-sha2-256,hmac-sha2-512

# Schlüsselalgorithmen
HostKeyAlgorithms ssh-ed25519,ssh-rsa,ecdsa-sha2-nistp256,ecdsa-sha2-nistp384,ecdsa-sha2-nistp521

# Weitere Sicherheitsoptionen
LoginGraceTime 1m             # Zeitfenster für die Anmeldung begrenzen
MaxAuthTries 3                # Maximale Anzahl von Versuchen für die Authentifizierung begrenzen
MaxSessions 2                 # Maximale Anzahl gleichzeitiger Sitzungen pro Benutzer begrenzen

# Protokollierung
LogLevel VERBOSE               # Detaillierte Protokollierung für mehr Sicherheit

# Deaktiviere nicht benötigte Optionen
UseDNS no                      # DNS-Lookups deaktivieren
PrintMotd no                   # Message of the Day nicht anzeigen
PrintLastLog no                # Letzten Anmeldeversuch nicht anzeigen
TCPKeepAlive yes               # TCP-KeepAlive aktivieren
UsePAM no                      # PAM deaktivieren

# Schlüsselbasierte Authentifizierung erzwingen
PubkeyAuthentication yes       # Öffentliche Schlüssel für die Authentifizierung zulassen
```

Stellen Sie sicher, dass Sie nach der Bearbeitung der Konfigurationsdatei den SSH-Server neu starten:

```bash
sudo service ssh restart
```

# Fail2Ban

Es ist eine gute Idee, Fail2Ban zu verwenden, um Brute-Force-Angriffe auf Ihren SSH-Server zu erkennen und die betroffenen IP-Adressen zu blockieren. Hier sind die Schritte, um Fail2Ban zu konfigurieren:

1. **Fail2Ban installieren:**

   ```bash
   sudo apt-get update
   sudo apt-get install fail2ban
   ```

2. **Fail2Ban-Konfigurationsdatei bearbeiten:**

   Öffnen Sie die Fail2Ban-Konfigurationsdatei in einem Texteditor Ihrer Wahl:

   ```bash
   sudo nano /etc/fail2ban/jail.local
   ```

   Fügen Sie die folgenden Zeilen hinzu, um eine benutzerdefinierte Regel für SSH-Brute-Force-Angriffe hinzuzufügen:

   ```plaintext
   [sshd]
   enabled = true
   port = ssh
   filter = sshd
   logpath = /var/log/auth.log
   maxretry = 3
   findtime = 600
   bantime = 900
   banaction = ufw
   ```

   - `maxretry`: Maximale Anzahl von Fehlversuchen, bevor eine IP-Adresse gesperrt wird.
   - `findtime`: Zeitfenster, in dem `maxretry`-Versuche stattfinden müssen, damit eine IP-Adresse gesperrt wird (in Sekunden).
   - `bantime`: Dauer der Sperrung einer IP-Adresse (in Sekunden).

3. **Fail2Ban-Regeln neu laden:**

   Nachdem Sie die Konfiguration geändert haben, laden Sie die Fail2Ban-Regeln neu:

   ```bash
   sudo service fail2ban restart
   ```

4. **Überprüfen Sie die Fail2Ban-Protokolle:**

   Sie können die Protokolle von Fail2Ban überprüfen, um sicherzustellen, dass es funktioniert und IP-Adressen bei wiederholten Fehlversuchen sperrt:

   ```bash
   sudo tail /var/log/fail2ban.log
   ```

Das oben stehende Beispiel blockiert eine IP-Adresse für 15 Minuten (900 Sekunden) nach drei fehlgeschlagenen Anmeldeversuchen innerhalb eines Zeitfensters von 10 Minuten (600 Sekunden). 

# UFW

UFW (Uncomplicated Firewall) ist ein einfaches Werkzeug zum Konfigurieren von Firewalls unter Linux, und es kann eine zusätzliche Sicherheitsebene für Ihren Server bieten. Hier sind die Schritte, um UFW zu installieren und zu konfigurieren:

1. **UFW-Konfiguration für SSH zulassen:**

   ```bash
   sudo ufw allow ssh
   ```

   Dies öffnet den SSH-Port (standardmäßig Port 22) und ermöglicht SSH-Verbindungen.

2. **UFW für Fail2Ban konfigurieren:**

   Stellen Sie sicher, dass Fail2Ban mit UFW zusammenarbeitet, um gesperrte IP-Adressen korrekt zu blockieren. Ändern Sie die Datei `/etc/fail2ban/action.d/ufw.conf` und setzen Sie `banaction` auf `ufw`:

   ```bash
   sudo nano /etc/fail2ban/action.d/ufw.conf
   ```

   Fügen Sie die folgende Zeile hinzu:

   ```plaintext
   banaction = ufw
   ```

4. **UFW aktivieren:**

   Aktivieren Sie UFW, um die Firewall zu starten:

   ```bash
   sudo ufw enable
   ```

   Bestätigen Sie die Aufforderung mit "y".

5. **UFW-Status überprüfen:**

   Überprüfen Sie den Status der UFW-Firewall:

   ```bash
   sudo ufw status
   ```

   Es sollte anzeigen, dass SSH erlaubt ist.

# IP Adressen entbannen

Hier ist ein einfaches Bash-Skript, um eine IP-Adresse aus der Fail2Ban-Sperre zu entfernen. Das Skript nimmt die zu entsperrende IP-Adresse als Argument:
![](../Assets/unban_ip.sh)
```bash
#!/bin/bash

# Überprüfen, ob eine IP-Adresse als Argument übergeben wurde
if [ -z "$1" ]; then
    echo "IP-Adresse nicht angegeben."
    exit 1
fi

# IP-Adresse säubern und sicherstellen, dass nur gültige Zeichen enthalten sind
ip=$(echo "$1" | sed 's/[^0-9a-fA-F\.:]//g')

# Überprüfen, ob die IP-Adresse gültig ist
if ! [[ "$ip" =~ ^[0-9a-fA-F\.:]+$ ]]; then
    echo "Ungültige IP-Adresse."
    exit 1
fi

sudo /usr/bin/fail2ban-client set sshd unbanip $ip

# Ausgabe, dass die IP-Adresse entsperrt wurde
echo "IP-Adresse $ip wurde entsperrt."
```

Speichern Sie das Skript in einer Datei, z. B. `unban_ip.sh`, und geben Sie die ausführbaren Berechtigungen:

```bash
chmod +x unban_ip.sh
```

Um sicherzustellen, dass jeder die Datei `unban_ip.sh` lesen und ausführen kann, aber nur root die Berechtigung zum Ändern hat, können Sie die `chmod`-Befehle wie folgt verwenden:

```bash
# Lesen und Ausführen für alle
sudo chmod 555 unban_ip.sh

# Verändern (Schreiben) nur für root
sudo chown root:root unban_ip.sh
sudo chmod 755 unban_ip.sh  # oder chmod u+w,go-w unban_ip.sh
```

Hier wird `chmod 555` verwendet, um Lese- und Ausführungsrechte für alle (Owner, Group und Others) zu setzen. Dies bedeutet, dass jeder die Datei lesen und ausführen kann, aber keine Schreibberechtigungen hat.

Der anschließende `chown`-Befehl wird verwendet, um den Besitzer der Datei auf root zu setzen, und `chmod 755` wird verwendet, um die Berechtigungen für root so zu setzen, dass die Datei von root auch bearbeitet werden kann. Alternativ können Sie auch `chmod u+w,go-w` verwenden, um dem Eigentümer (root) Schreibberechtigungen zu geben und diese für die Gruppe und Andere zu entfernen.

Stellen Sie sicher, dass Sie diese Befehle mit den entsprechenden Rechten ausführen, da die Datei sonst nicht von anderen Benutzern bearbeitet werden kann.

Dann können Sie das Skript wie folgt ausführen, um eine bestimmte IP-Adresse zu entsperren:

```bash
./unban_ip.sh 123.456.789.123
```

Das Skript verwendet `fail2ban-client`, um die IP-Adresse aus der Fail2Ban-Sperrliste zu entfernen. Stellen Sie sicher, dass Sie das Skript mit den erforderlichen Berechtigungen ausführen, um auf `fail2ban-client` zugreifen zu können.

## sudoers anpassen

Um den Befehl `/usr/bin/fail2ban-client set sshd unbanip $ip` für alle Benutzer als `sudo` ausführbar zu erlauben, müssen Sie eine entsprechende Eintragung in der sudoers-Konfigurationsdatei vornehmen. Hier sind die Schritte, die Sie befolgen können:

1. Öffnen Sie die sudoers-Konfigurationsdatei in einem Texteditor:

```bash
sudo visudo
```

2. Fügen Sie am Ende der Datei die folgende Zeile hinzu:

```plaintext
ALL ALL=(ALL:ALL) NOPASSWD: /usr/bin/fail2ban-client set sshd unbanip *
```

Die Zeile besagt, dass alle Benutzer (`ALL`) auf allen Hosts (`ALL`) die Berechtigung haben, ohne Passwortabfrage (`NOPASSWD`) den angegebenen Befehl auszuführen. Der Platzhalter `*` steht dabei für alle möglichen IP-Adressen, die in den Befehl übergeben werden können.

3. Speichern Sie die Datei und schließen Sie den Editor.

# IP Adresse selbst entbannen

Um das Bash-Skript `unban_ip.sh` von einer PHP-Seite aus aufzurufen und das Ergebnis zurückzugeben, können Sie die Funktion `exec` verwenden. Hier ist eine einfache PHP-Seite:
![](../Assets/unban.php)
```php
<?php

// Sicherheitsüberprüfung: Überprüfen, ob die IP-Adresse als Parameter übergeben wurde
if (isset($_GET['unban'])) {
    $ip = $_SERVER['REMOTE_ADDR'];
    
    // Überprüfen, ob die IP-Adresse gültig ist
    if (filter_var($ip, FILTER_VALIDATE_IP)) {
        // Nur zulässige Zeichen für die IP-Adresse zulassen
        $ip = preg_replace('/[^0-9a-fA-F\.:]/', '', $ip);

        // Aufruf des Bash-Skripts mit der IP-Adresse als Argument
        $command = "./unban_ip.sh $ip";
        exec($command, $output, $returnCode);
        
        // Überprüfen, ob der Befehl erfolgreich ausgeführt wurde
        if ($returnCode === 0) {
            // Ausgabe des Ergebnisses
            echo "IP-Adresse $ip wurde entsperrt. Ergebnis: " . implode("\n", $output);
        } else {
            echo "Fehler beim Entbannen der IP-Adresse.";
        }
    } else {
        echo "Ungültige IP-Adresse.";
    }
} else {
    echo "IP-Adresse nicht angegeben.";
}

?>
```

Dieses PHP-Skript ruft das Bash-Skript `unban_ip.sh` auf und gibt das Ergebnis zurück. Beachten Sie erneut, dass die Sicherheit dieser Methode von entscheidender Bedeutung ist. Stellen Sie sicher, dass das Skript `unban_ip.sh` nur von autorisierten Benutzern mit den erforderlichen Berechtigungen ausgeführt werden kann.

Darüber hinaus sollten Sie alle Zugriffe auf diese Funktion sorgfältig überwachen und sicherstellen, dass sie nicht missbraucht wird.

# Webserver installieren

Um ein minimales Setup für PHP mit Nginx zu erstellen, benötigst du eine Installation von PHP und Nginx auf deinem Server. Hier sind die grundlegenden Schritte:

1. **PHP und Nginx installieren:**

   Installiere PHP und Nginx auf deinem Server. Hier ist ein Beispiel für Debian/Ubuntu:

   ```bash
   sudo apt update
   sudo apt install php-fpm nginx
   ```

2. **Nginx-Konfiguration:**

   Erstelle eine einfache Nginx-Konfigurationsdatei für dein PHP-Projekt.
```nginx
server {
    listen 80;
    server_name example.com;
    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl;
    server_name example.com;

    ssl_certificate /etc/letsencrypt/live/example.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/example.com/privkey.pem;
    ssl_trusted_certificate /etc/letsencrypt/live/example.com/chain.pem;
	include /etc/letsencrypt/options-ssl-nginx.conf;

    root /var/www/html;
    index index.php index.html index.htm;

    location / {
        try_files $uri $uri/ /index.php?$args;
    }

    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/var/run/php/php7.4-fpm.sock;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }

    location ~ /\. {
        deny all;
        access_log off;
        log_not_found off;
    }

    location = /favicon.ico {
        log_not_found off;
        access_log off;
    }

    location = /robots.txt {
        allow all;
        log_not_found off;
        access_log off;
    }

    location ~* \.(css|gif|ico|jpeg|jpg|js|png)$ {
        expires max;
        log_not_found off;
    }

    # Deny access to sensitive files
    location ~* \.(htaccess|htpasswd|ini|log|sh|inc|bak|swp|dist)$ {
        deny all;
        access_log off;
        log_not_found off;
    }

    # Block common exploits and threats
    include snippets/block-exploits.conf;

    # Block known bad bots
    include snippets/block-bots.conf;

    # Block access to wp-config.php and .htpasswd
    location ~* (wp-config.php|\.htpasswd) {
        deny all;
        access_log off;
        log_not_found off;
    }

    # Block access to xmlrpc.php
    location = /xmlrpc.php {
        deny all;
        access_log off;
        log_not_found off;
    }
}

```
   
   Ersetze `example.com` durch deine eigene Domain oder IP-Adresse und passe die Pfade an, wenn nötig.

1. **PHP-Konfiguration:**

   Passe die PHP-Konfiguration an, um sicherzustellen, dass alles ordnungsgemäß funktioniert. In der Regel wird die PHP-Konfigurationsdatei unter `/etc/php/7.4/fpm/php.ini` oder ähnlich gespeichert sein.

   Überprüfe und passe gegebenenfalls Einstellungen wie `cgi.fix_pathinfo`, `upload_max_filesize`, und `post_max_size` an.

2. **Webserver neu starten:**

   Starte den Nginx- und PHP-FPM-Service neu:

   ```bash
   sudo service nginx restart
   sudo service php7.4-fpm restart
   ```


Um die `unban.php`-Datei in das Webverzeichnis `/var/www/html` zu kopieren und über die URL `http://example.com/unban.php` aufzurufen, kannst du die folgenden Schritte ausführen:

1. Kopiere die `unban.php`-Datei nach `/var/www/html`:

   ```bash
   sudo cp unban.php /var/www/html/
   ```

   Hierbei wird davon ausgegangen, dass sich die `unban.php`-Datei im aktuellen Arbeitsverzeichnis befindet. Passe den Pfad entsprechend an, wenn sich die Datei an einem anderen Ort befindet.

2. Ändere die Berechtigungen, um sicherzustellen, dass der Webserver auf die Datei zugreifen kann:

   ```bash
   sudo chown www-data:www-data /var/www/html/unban.php
   ```

   Die genannten Berechtigungen setzen voraus, dass der Webserver-Benutzer als `www-data` konfiguriert ist. Ändere dies entsprechend, wenn dein Webserver einen anderen Benutzer verwendet.

3. Überprüfe die Dateiberechtigungen:

   ```bash
   ls -l /var/www/html/unban.php
   ```

   Stelle sicher, dass die Datei für den Webserver-Benutzer (`www-data`) lesbar ist.

4. Rufe die Datei über den Webbrowser auf:

   Öffne deinen Webbrowser und gehe zu `http://example.com/unban.php`. Ersetze `example.com` durch deine tatsächliche Domain oder IP-Adresse.

Das sollte es sein! Du kannst jetzt die `unban.php`-Datei über die angegebene URL aufrufen. Beachte, dass der Aufruf der Datei über HTTP erfolgt, da du bisher keine SSL-Konfiguration für diese URL erstellt hast. Wenn du SSL verwenden möchtest, musst du die vorherige Nginx-Konfiguration entsprechend aktualisieren.

# SSL Zertifikat installieren

Um ein Let's Encrypt-Zertifikat für deine Nginx-Konfiguration zu erhalten, kannst du den sogenannten Certbot verwenden, der ein einfach zu bedienendes Tool für die Automatisierung des Let's Encrypt-Zertifikatserwerbs und -verlängerung ist. Hier sind die Schritte:

1. **Certbot installieren:**

   Installiere Certbot auf deinem Server. Für Debian/Ubuntu:

   ```bash
   sudo apt update
   sudo apt install certbot
   ```

2. **SSL-Konfiguration in Nginx vorbereiten:**

   Stelle sicher, dass die SSL-Konfiguration in deiner Nginx-Konfiguration bereit ist, wie im vorherigen Beispiel gezeigt.

3. **Let's Encrypt-Zertifikat erhalten:**

   Führe Certbot aus, um ein Zertifikat zu erhalten:

   ```bash
   sudo certbot certonly --nginx -d example.com
   ```

   Ersetze `example.com` durch deine tatsächliche Domain.

   Certbot wird dich nach weiteren Informationen fragen und anschließend das Zertifikat in einem Verzeichnis speichern, das in deiner Nginx-Konfiguration referenziert wird.

4. **SSL-Konfiguration in Nginx aktualisieren:**

   Passe deine Nginx-Konfiguration an, um das neu erhaltene Zertifikat zu verwenden. Aktualisiere die folgenden Zeilen in deiner Nginx-Konfiguration:

   ```nginx
   ssl_certificate /etc/letsencrypt/live/example.com/fullchain.pem;
   ssl_certificate_key /etc/letsencrypt/live/example.com/privkey.pem;
   ssl_trusted_certificate /etc/letsencrypt/live/example.com/chain.pem;
   ```

   Ersetze `example.com` durch deine tatsächliche Domain.

5. **Nginx-Service neu starten:**

   Nachdem du deine Nginx-Konfiguration aktualisiert hast, starte den Nginx-Service neu, damit die Änderungen wirksam werden:

   ```bash
   sudo service nginx restart
   ```

Das ist es! Deine Nginx-Konfiguration sollte jetzt SSL verwenden, und du kannst auf deine Website über eine sichere HTTPS-Verbindung zugreifen. 

6. **Automatische Verlängerung des SSL-Zertifikats**
   Um sicherzustellen, dass Certbot automatisch dein Let's Encrypt-Zertifikat erneuert, kannst du einen Cron-Job einrichten. Certbot speichert normalerweise eine Cron-Job-Aufgabe, um die Zertifikate regelmäßig zu überprüfen und zu erneuern.

   Du kannst überprüfen, ob Certbot bereits einen Cron-Job für die automatische Verlängerung erstellt hat. Führe den folgenden Befehl aus:

   ```bash
   sudo crontab -l
   ```

   Wenn Certbot bereits eine Zeile für die automatische Erneuerung enthält, ist die Einrichtung bereits erfolgt. Du kannst sie dann einfach so belassen.

2. **Hinzufügen eines Certbot-Cron-Jobs:**

   Falls Certbot noch keinen automatischen Cron-Job erstellt hat oder wenn du sicherstellen möchtest, dass es richtig konfiguriert ist, füge die Aufgabe manuell hinzu:

   ```bash
   sudo crontab -e
   ```

   Füge die folgende Zeile hinzu, um Certbot zweimal täglich (um 2:30 Uhr und 14:30 Uhr) nach Aktualisierungen suchen zu lassen:

   ```bash
   30 2,14 * * * /usr/bin/certbot renew --quiet
   ```

   Speichere und schließe die Datei.

   Diese Zeile führt `certbot renew` zweimal täglich aus, und wenn ein Zertifikat in den letzten 30 Tagen abgelaufen ist, wird es automatisch erneuert. Der Parameter `--quiet` unterdrückt die Ausgabe, es sei denn, ein erneutes Laden ist erforderlich oder ein Fehler tritt auf.

# Flag erstellen

Um ein Skript zu erstellen, das alle 24 Stunden eine neue zufällige Flagge erstellt und sie im Home-Verzeichnis eines bestimmten Benutzers speichert, könntest du ein Bash-Skript verwenden. 
![](../Assets/flag.sh)
```bash
#!/bin/env bash
# Überprüfe, ob beide Argumente übergeben wurden
if [ "$#" -ne 2 ]; then
  echo "Bitte Benutzernamen und Home-Verzeichnis als Argumente angeben."
  exit 1
fi
 
# Benutzername des zweiten Benutzers
ctf_user="$1"

# Pfad zum Home-Verzeichnis des zweiten Benutzers
ctf_user_home="$2"
 
# Name der Flagge-Datei
flag_file="flag.txt"

# Funktion zum Generieren einer zufälligen Flagge
generate_random_flag() {
  # Erstelle eine zufällige Zeichenkette
  random_string=$(openssl rand -base64 16 | tr -dc 'a-zA-Z0-9' | head -c 32)
  # Füge Datum und Uhrzeit hinzu
  timestamp=$(date +"%Y%m%d%H%M%S")
  flag="MR{$random_string}"
  echo "$flag"
}

# Generiere eine neue zufällige Flagge
new_flag=$(generate_random_flag)

# Erstelle die Flagge-Datei im Home-Verzeichnis des Benutzers
flag_path="$ctf_user_home/$flag_file"
echo "$new_flag" > "$flag_path"

# Setze Berechtigungen, damit nur der Benutzer und der Root-Benutzer die Datei lesen können
chmod 600 "$flag_path"

# Setze den Besitzer der Datei auf den zweiten Benutzer
chown "$ctf_user:$ctf_user" "$flag_path"

# Erstelle eine Sicherung der Flagge mit Zeitstempel in /root/recent_flags.txt 
echo "$(date +"%Y-%m-%d %H:%M:%S") - $new_flag" >> "/root/recent_flags.txt"

echo "Neue Flagge erstellt und im Home-Verzeichnis von $ctf_user gespeichert: $flag_path"
echo "Die neue Flagge lautet und ist bis 24Uhr gültig\n ${new_flag}"
```

Füge dieses Skript zu deinem System hinzu und führe es alle 24 Stunden über einen Cron-Job aus. Zum Beispiel:

```bash
0 0 * * * /root/flag.sh user path
```

Dieser Cron-Job wird das Skript täglich um Mitternacht ausführen. Stelle sicher, dass der Pfad zum Skript korrekt ist.