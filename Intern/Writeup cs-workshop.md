Wir finden in der E-Mail `E-Mail-Postfach/Eingegangen/09-01-24-11-38.txt` den Hinweis auf einen *web-access.txt* welche zunächst dekodiert werden muss.

```bash
cs-workshop@mindrefined:~$ cat E-Mail-Postfach/Eingegangen/09-01-24-11-38.txt
SENDER:    Eckstein, Franziska <f.eckstein@shs.de>
EMPFÄNGER: Förstner, Alex <a.foerstner@shs.de>
BETREFF:   Antrangsvorgang für Geschäftsreisen und Urlaub

Sehr geehrter Herr Förstner,

Denken Sie daran, Ihre Urlaubsanträge sowie Geschäftsreisen rechtzeitig 14 
Werktage vor Antritt auf unserer internen Website zu beantragen. Falls es Ihnen 
entfallen ist, finden Sie das dafür notwendige, persönliches Passwort in der Datei
web-access.txt. Denken Sie an die Dekodierung vor der Verwendung. Bitte ändern Sie 
dieses Passwort nicht und geben Sie es auch an niemanden weiter.


Mit freundlichen Grüßen


F. Eckstein, Head of Sales
```

In der Datei `E-Mail-Postfach/Eingegangen/04-01-24-11-06.txt` ist ein wichtiger Hinweis auf das Passwort enthalten, den wir genauer untersuchen sollten.

```bash
cs-workshop@mindrefined:~$ cat E-Mail-Postfach/Eingegangen/04-01-24-11-06.txt
SENDER:    Kempf, Joachim <j.kempf@shs.de>
EMPFÄNGER: Förstner, Alex <a.foerstner@shs.de>
BETREFF:   AW: AW: AW: AW: AW:


Hallo Alex!

Auf deiner Workstation sollte eine Datei documents.zip abgelegt sein, in der das 
Passwort gespeichert ist. Wenn du deinen Website Zugang hast, solltest du damit in 
der Lage sein, die Datei zu öffnen. Der Inhalt ermöglicht dir dann vom Sales Team 
verschlüsselte Dateien zu öffnen.

Viele Grüße,

Joachim
```

Es scheint, als würde sich das gesuchte Passwort in der Datei *documents.zip* befinden. Um diese Datei zu finden, begeben wir uns in den Ordner "Dokumente".

```bash
cs-workshop@mindrefined:~/Dokumente$ find ~/ -iname documents.zip
/home/cs-workshop/Dokumente/documents.zip
```

Der zweite Hinweis bezieht sich auf einen Web-Zugang. In dem Ordner "Dokumente" suchen wir nach der Datei *web-access.txt*.

```bash
cs-workshop@mindrefined:~/Dokumente$ ls -lah
total 20K
drwx------  2 cs-workshop cs-workshop 4.0K Jan 24 01:28 .
drwxr-xr-x 11 cs-workshop cs-workshop 4.0K Jan 24 03:43 ..
-rwx------  1 cs-workshop cs-workshop 6.7K Jan 24 01:27 documents.zip
-rwx------  1 cs-workshop cs-workshop   17 Jan 23 17:09 web-access.txt
```

Die Datei *web-access.txt* ist offenbar in Base64 codiert, was durch die beiden *=* am Ende erkennbar ist.

```bash
cs-workshop@mindrefined:~/Dokumente$ cat web-access.txt
bVhEQXdWcUVUVg==
```

Die Decodierung dieser Datei führen wir mit dem folgenden Befehl durch:

```bash
cs-workshop@mindrefined:~/Dokumente$ cat web-access.txt | base64 -d
mXDAwVqETV
```

Mit dem nun erhaltenen Passwort *mXDAwVqETV* können wir die Datei *documents.zip* entpacken.

```bash
cs-workshop@mindrefined:~/Dokumente$ unzip documents.zip
Archive:  documents.zip
[documents.zip] Datenschutzerklaerung.txt password:
  inflating: Datenschutzerklaerung.txt
 extracting: Personalfragebogen_Foerstner_Alex.txt.gpg
  inflating: Alex_Foerstner_0x27EEC5DA_SECRET.asc
 extracting: sales-access.txt
  inflating: Verschwiegenheitserklaerung.txt
```

Das Passwort für den Sales-Zugang befindet sich in der Datei *sales-access.txt*.

Um den Personalfragebogen von Alex Förstner zu erhalten, nutzen wir *gpg* und den dazugehörigen Schlüssel *Alex_Foerstner_0x27EEC5DA_SECRET.asc*. Nachdem wir den Personalfragebogen entschlüsselt haben, finden wir Alex' Personalnummer darin.