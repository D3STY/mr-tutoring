![](../Assets/Pasted%20image%2020240205222234.png)

[HoF — Vulnlab](https://www.vulnlab.com/hof)

[Machines — Vulnlab](https://www.vulnlab.com/machines)

| Machine / Chain / Lab                                | Erledigt          |
|----------------------------------------|-------------------|
| ❌ Flagsalad                            | Nicht erledigt    |
| ❌ Share                                | Nicht erledigt    |
| ✅ Warmup_1                             | Erledigt          |
| ✅ Warmup_2                             | Erledigt          |
| ❌ Warmup_3                             | Nicht erledigt    |
| ❌ Atlas_Root                           | Nicht erledigt    |
| ❌ Atlas_User                           | Nicht erledigt    |
| ✅ Baby_Root                            | Erledigt          |
| ✅ Baby_User                            | Erledigt          |
| ✅ Baby2_Root                           | Erledigt          |
| ✅ Baby2_User                           | Erledigt          |
| ✅ Bamboo_Root                          | Erledigt          |
| ✅ Bamboo_User                          | Erledigt          |
| ✅ Breach_Root                          | Erledigt          |
| ✅ Breach_User                          | Erledigt          |
| ✅ Bruno_Root                           | Erledigt          |
| ✅ Bruno_User                           | Erledigt          |
| ❌ Control_Root                         | Nicht erledigt    |
| ❌ Control_User-1                       | Nicht erledigt    |
| ❌ Control_User-2                       | Nicht erledigt    |
| ✅ Data_Root                            | Erledigt          |
| ✅ Data_User                            | Erledigt          |
| ✅ Delegate_Root                        | Erledigt          |
| ✅ Delegate_User                        | Erledigt          |
| ❌ Dump_Root                            | Nicht erledigt    |
| ❌ Dump_User                            | Nicht erledigt    |
| ❌ Escape_Root                          | Nicht erledigt    |
| ❌ Escape_User                          | Nicht erledigt    |
| ✅ Feedback_Root                        | Erledigt          |
| ✅ Forgotten_Root                       | Erledigt          |
| ✅ Forgotten_User                       | Erledigt          |
| ✅ Hybrid_Root                          | Erledigt          |
| ✅ Hybrid_User-1                        | Erledigt          |
| ✅ Hybrid_User-2                        | Erledigt          |
| ✅ Intercept_Root                       | Erledigt          |
| ✅ Intercept_User                       | Erledigt          |
| ✅ Job_Root                             | Erledigt          |
| ✅ Job_User                             | Erledigt          |
| ✅ Job2_Root                            | Erledigt          |
| ✅ Job2_User                            | Erledigt          |
| ✅ Kaiju_Root                           | Erledigt          |
| ✅ Kaiju_User-1                         | Erledigt          |
| ✅ Kaiju_User-2                         | Erledigt          |
| ✅ Lock_Root                            | Erledigt          |
| ✅ Lock_User                            | Erledigt          |
| ✅ Lustrous_Root                        | Erledigt          |
| ✅ Lustrous_User                        | Erledigt          |
| ✅ Media_Root                           | Erledigt          |
| ✅ Media_User                           | Erledigt          |
| ✅ Push_Root                            | Erledigt          |
| ✅ Push_User-1                          | Erledigt          |
| ✅ Push_User-2                          | Erledigt          |
| ❌ Race_Root                            | Nicht erledigt    |
| ❌ Race_User                            | Nicht erledigt    |
| ❌ Rainbow_Root                         | Nicht erledigt    |
| ❌ Rainbow_User                         | Nicht erledigt    |
| ❌ Rainbow2_Root                        | Nicht erledigt    |
| ❌ Reaper_Root                          | Nicht erledigt    |
| ❌ Reaper_User                          | Nicht erledigt    |
| ✅ Reflection-DC01_Root                 | Erledigt          |
| ✅ Reflection-MS01_User                 | Erledigt          |
| ✅ Reflection-WS01_User                 | Erledigt          |
| ✅ Retro_Root                           | Erledigt          |
| ❌ Sidecar_Root                         | Nicht erledigt    |
| ❌ Sidecar_User                         | Nicht erledigt    |
| ✅ Slonik_Root                          | Erledigt          |
| ✅ Slonik_User                          | Erledigt          |
| ❌ Store_Root                           | Nicht erledigt    |
| ❌ Store_User                           | Nicht erledigt    |
| ✅ Sync_Root                            | Erledigt          |
| ✅ Sync_User                            | Erledigt          |
| ✅ Tea_Root                             | Erledigt          |
| ✅ Tea_User-1                           | Erledigt          |
| ✅ Tea_User-2                           | Erledigt          |
| ✅ Trusted_Root                         | Erledigt          |
| ✅ Trusted_User                         | Erledigt          |
| ✅ Unchained_Root                       | Erledigt          |
| ✅ Unchained_User                       | Erledigt          |
| ❌ Zero_Root                            | Nicht erledigt    |
| ❌ Zero_User                            | Nicht erledigt    |
| ❌ Shinra_And_So_It_Begins              | Nicht erledigt    |
| ❌ Shinra_Carousel                      | Nicht erledigt    |
| ❌ Shinra_Crypto_Is_Fun                 | Nicht erledigt    |
| ❌ Shinra_Dominance                     | Nicht erledigt    |
| ❌ Shinra_Heavens_Gate                   | Nicht erledigt    |
| ❌ Shinra_I_Heard_You_Like_JS           | Nicht erledigt    |
| ❌ Shinra_Know_Your_Planets             | Nicht erledigt    |
| ❌ Shinra_Lost_And_Found                 | Nicht erledigt    |
| ❌ Shinra_Master                         | Nicht erledigt    |
| ❌ Shinra_Old_Is_New                     | Nicht erledigt    |
| ❌ Shinra_One_More_Time                  | Nicht erledigt    |
| ❌ Shinra_Those_Pesky_Humans             | Nicht erledigt    |
| ❌ Shiva_Breached                        | Nicht erledigt    |
| ❌ Shiva_Classico                        | Nicht erledigt    |
| ❌ Shiva_Drop_D                          | Nicht erledigt    |
| ❌ Shiva_Exposed                         | Nicht erledigt    |
| ❌ Shiva_Forgotten                       | Nicht erledigt    |
| ❌ Shiva_Kerberos                        | Nicht erledigt    |
| ❌ Shiva_Level_Up                        | Nicht erledigt    |
| ❌ Shiva_Master                          | Nicht erledigt    |
| ❌ Shiva_Open_Door_Policy                | Nicht erledigt    |
| ❌ Shiva_Refreshments                    | Nicht erledigt    |
| ❌ Shiva_Side_Quest                      | Nicht erledigt    |
| ❌ Shiva_The_Hunt_Begins                  | Nicht erledigt    |
| ❌ Shiva_Unpleasant                      | Nicht erledigt    |
| ✅ Wutai_A_New_Journey                   | Erledigt          |
| ❌ Wutai_Been_There_Done_That             | Nicht erledigt    |
| ❌ Wutai_Digging_Deeper                   | Nicht erledigt    |
| ❌ Wutai_Dominance                       | Nicht erledigt    |
| ✅ Wutai_Hidden_In_Plain_Sight           | Erledigt          |
| ❌ Wutai_Humans                          | Nicht erledigt    |
| ❌ Wutai_Master                          | Nicht erledigt    |
| ✅ Wutai_That_Escalated_Quickly          | Erledigt          |
| ❌ Wutai_This_Is_Fine                     | Nicht erledigt    |
| ❌ Wutai_Well_Managed                    | Nicht erledigt    |
| ✅ Wutai_Working_As_Intended             | Erledigt          |
| ❌ Wutai_You_Must_Be_Kidding              | Nicht erledigt    |
