Das Vulnlab Wiki ist extrem wichtig. Hier finden wir *alle* benötigten Infos.
[Vulnlab Wiki](https://wiki.vulnlab.com/)

In den Recommendations finden wir eine Lister aller Maschinen, Chains und Labs:
[Recommendations - Vulnlab Wiki](https://wiki.vulnlab.com/intro/recommendations)

Zu allen Boxen gibt es Hinweise und auch komplette Walkthroughs:
[Hints & Walkthroughs - Vulnlab Wiki](https://wiki.vulnlab.com/hints-and-walkthroughs)

## Vorbereitung
1. Discord Server beitreten
	https://discord.gg/vulnlab
	
	![](../Assets/spaces_I3I73FFqB6GvT8N5Mt1N_uploads_Geh1bfdWIO8ZUDRaRPGG_lab_accept_rules.webp)

2. Voucher einlösen
	Wenn du ein 90-Tage-Voucher erhalten hast, kannst du den Befehl */redeem* verwenden, um ihn zu aktivieren (anstatt von */register*).
	
	![](../Assets/spaces_I3I73FFqB6GvT8N5Mt1N_uploads_gnYcaD2QuT1wQG0vq4jB_redeem_voucher%201.webp)
	
	Danach kannst du alle `/`-Befehle nutzen, die der Bot anbietet. Der erste, den du verwenden möchtest, ist `/vpn`, um die VPN-Pakete zu erhalten. Dies bietet derzeit 2 verschiedene VPN-Pakete an - eines für eigenständige Maschinen und Ketten (endet in aws) und eines für die Red-Team-Labore (endet in rtl).

3. VPN Runterladen
	
	![](../Assets/spaces_I3I73FFqB6GvT8N5Mt1N_uploads_WZ1EPMzFy1MFVhuiVuLm_lab_vpn%202.webp)
## Verbinden
1. Mit dem VPN Verbinden (AWS)
   
```
┌──(desty㉿TP-MRTN)-[~]
└─$ sudo openvpn ~/VL/aws.ovpn
2024-01-30 11:44:28 Initialization Sequence Completed
```

# Eigenständige Maschinen & Chains 

1. Du kannst eine Liste aller Maschinen im Kanal #vl-lab sehen.

	![](../Assets/spaces_I3I73FFqB6GvT8N5Mt1N_uploads_ySru2qDYtt4F4ML0G9Vm_lab_machines%201.webp)

2. Nachdem du eine ausgewählt hast, die du bearbeiten möchtest, verwende den Befehl **/machine**, um das Bedienfeld für die Maschine aufzurufen.

	![](../Assets/spaces_I3I73FFqB6GvT8N5Mt1N_uploads_b3gpMAKXZB502bZUgUa1_lab_machine_details%201.webp)

3. Hier kannst du die Maschine starten, stoppen oder die Laufzeit verlängern. Die Standarddauer beträgt 2 Stunden pro Maschine. Danach wird sie heruntergefahren, es sei denn, du verlängerst die Laufzeit. Nach dem Drücken des Startknopfs wird eine IP-Adresse angezeigt, mit der du dich verbinden kannst, wenn du mit dem AWS-VPN verbunden bist.

4. Du kannst dieselben Schritte befolgen, um eine Kette von Maschinen mit dem Befehl **/chain** zu starten.

# Red Team Labs

Die Red Team Labs laufen rund um die Uhr. Bitte sieh in den dedizierten Kanälen für die Labs, #vl-shinra und #vl-wutai, nach Informationen zu den Einstiegspunkten und Regeln.