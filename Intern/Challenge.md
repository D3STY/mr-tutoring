
# Ordnerstruktur
```
/ctf_challenge
|-- README.txt
|-- security_layer_1
|   |-- hint1.txt
|   |-- file1.txt
|   |-- base64_encoded.txt
|-- security_layer_2
|   |-- hint2.txt
|   |-- file2.txt
|   |-- password_protected.zip
|   |-- password_protected
|       |-- hint3.txt
|       |-- pgp_key
|-- security_layer_3
|   |-- hint4.txt
|   |-- pgp_encrypted.txt
|   |-- flag.txt
```

Hier ist eine kurze Beschreibung der Dateien und Ordner:

- **ctf_challenge:** Das Hauptverzeichnis für deine Challenge.
  - **README.txt:** Allgemeine Informationen zur Challenge.

- **security_layer_1:** Erste Sicherheitsebene.
  - **hint1.txt:** Hinweis für den ersten Schritt.
  - **file1.txt:** Normale Datei für Ablenkung.
  - **base64_encoded.txt:** Datei, die in Base64 codiert ist. Der Benutzer muss dies decodieren.

- **security_layer_2:** Zweite Sicherheitsebene.
  - **hint2.txt:** Hinweis für den nächsten Schritt.
  - **file2.txt:** Weitere Ablenkungsdatei.
  - **password_protected.zip:** ZIP-Archiv, das passwortgeschützt ist. Der Benutzer muss das Passwort herausfinden, um die Datei zu extrahieren.
  - **protected_directory:** Ein geschütztes Verzeichnis.
    - **hint3.txt:** Hinweis im geschützten Verzeichnis.
    - **pgp_key:** Der PGP Schlüssel, welcher für **pgp_encrypted.txt** benötigt wird

- **security_layer_3:** Letzte Sicherheitsebene.
  - **hint4.txt:** Hinweis für den finalen Schritt um die PGP Datei auf Level 2 zu entschlüsseln.
  - **pgp_encrypted.txt:** Datei, die mit PGP verschlüsselt ist. Der Benutzer muss die Datei entschlüsseln.
  - **flag.txt:** Die Datei, die der Benutzer finden und entschlüsseln muss, um die Flagge zu erhalten.

Um sicherzustellen, dass alle Dateien gegen versehentliches Löschen geschützt sind, kannst  du das "immutable" (unveränderliche) Flag auf den Dateien setzen. Dies verhindert, dass die Dateien gelöscht oder überschrieben werden. 

Setze das "immutable" Flag für alle Dateien in deiner Ordnerstruktur mit dem Befehl `chattr +i`. Zum Beispiel:

```bash
chattr +i /ctf_challenge/security_layer_1/hint1.txt
chattr +i /ctf_challenge/security_layer_1/file1.txt
chattr +i /ctf_challenge/security_layer_1/base64_encoded.txt
# Führe diese Befehle für alle Dateien in deiner Struktur aus
```

Um das "immutable" Flag zu entfernen, verwende den Befehl `chattr -i`:

```bash
chattr -i /ctf_challenge/security_layer_1/hint1.txt
chattr -i /ctf_challenge/security_layer_1/file1.txt
chattr -i /ctf_challenge/security_layer_1/base64_encoded.txt
# Führe diese Befehle für alle Dateien in deiner Struktur aus
```

Es ist als zusätzliche Schutzschicht gegen versehentliches Löschen gedacht.