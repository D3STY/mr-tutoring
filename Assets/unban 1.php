<?php

// Sicherheitsüberprüfung: Überprüfen, ob die IP-Adresse als Parameter übergeben wurde
if (isset($_GET['unban'])) {
    $ip = $_SERVER['REMOTE_ADDR'];
    
    // Überprüfen, ob die IP-Adresse gültig ist
    if (filter_var($ip, FILTER_VALIDATE_IP)) {
        // Nur zulässige Zeichen für die IP-Adresse zulassen
        $ip = preg_replace('/[^0-9a-fA-F\.:]/', '', $ip);

        // Aufruf des Bash-Skripts mit der IP-Adresse als Argument
        $command = "./unban_ip.sh $ip";
        exec($command, $output, $returnCode);
        
        // Überprüfen, ob der Befehl erfolgreich ausgeführt wurde
        if ($returnCode === 0) {
            // Ausgabe des Ergebnisses
            echo "IP-Adresse $ip wurde entsperrt. Ergebnis: " . implode("\n", $output);
        } else {
            echo "Fehler beim Entbannen der IP-Adresse.";
        }
    } else {
        echo "Ungültige IP-Adresse.";
    }
} else {
    echo "IP-Adresse nicht angegeben.";
}

?>