#!/bin/env bash

# Überprüfe, ob beide Argumente übergeben wurden
if [ "$#" -ne 2 ]; then
  echo "Bitte Benutzernamen und Home-Verzeichnis als Argumente angeben."
  exit 1
fi

# Benutzername des zweiten Benutzers
ctf_user="$1"

# Pfad zum Home-Verzeichnis des zweiten Benutzers
ctf_user_home="$2"

# Name der Flagge-Datei
flag_file="flag.txt"

# Funktion zum Generieren einer zufälligen Flagge
generate_random_flag() {
  # Erstelle eine zufällige Zeichenkette
  random_string=$(openssl rand -base64 16 | tr -dc 'a-zA-Z0-9' | head -c 32)

  # Füge Datum und Uhrzeit hinzu
  timestamp=$(date +"%Y%m%d%H%M%S")
  flag="MR{$random_string}"

  echo "$flag"
}

# Generiere eine neue zufällige Flagge
new_flag=$(generate_random_flag)

# Erstelle die Flagge-Datei im Home-Verzeichnis des Benutzers
flag_path="$ctf_user_home/$flag_file"

echo "$new_flag" > "$flag_path"

# Setze Berechtigungen, damit nur der Benutzer und der Root-Benutzer die Datei lesen können
chmod 600 "$flag_path"

# Setze den Besitzer der Datei auf den zweiten Benutzer
chown "$ctf_user:$ctf_user" "$flag_path"

echo "Neue Flagge erstellt und im Home-Verzeichnis von $ctf_user gespeichert: $flag_path"
echo "Die neue Flagge lautet und ist bis 24Uhr gültig\n ${new_flag}"
