#!/bin/env bash

# Überprüfen, ob eine IP-Adresse als Argument übergeben wurde
if [ -z "$1" ]; then
    echo "IP-Adresse nicht angegeben."
    exit 1
fi

# IP-Adresse säubern und sicherstellen, dass nur gültige Zeichen enthalten sind
ip=$(echo "$1" | sed 's/[^0-9a-fA-F\.:]//g')

# Überprüfen, ob die IP-Adresse gültig ist
if ! [[ "$ip" =~ ^[0-9a-fA-F\.:]+$ ]]; then
    echo "Ungültige IP-Adresse."
    exit 1
fi

sudo /usr/bin/fail2ban-client set sshd unbanip $ip

# Ausgabe, dass die IP-Adresse entsperrt wurde
echo "IP-Adresse $ip wurde entsperrt."
